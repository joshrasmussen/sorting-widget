/** @jsx jsx */
import { ReactElement } from 'react'
import { jsx } from 'theme-ui'
import { Value } from './Value'
import { ValueContainer } from './ValueContainer'

type ScaleProps = {
  left?: number
  right?: number
  hidden?: boolean

  onDrag: (value?: number) => void
  onDrop: (side: 'left' | 'right') => void
}

function translateDist(
  left?: number,
  right?: number
): [number, number, number] {
  const correctedLeft = left || 0
  const correctedRight = right || 0

  if (correctedLeft > correctedRight) {
    return [50 - 25 * Math.sqrt(3), 25, -30]
  } else if (correctedLeft < correctedRight) {
    return [50 - 25 * Math.sqrt(3), -25, 30]
  } else {
    return [0, 0, 0]
  }
}

export function Scale(props: ScaleProps): ReactElement {
  const [dx, dy, deg] = translateDist(props.left, props.right)

  return (
    <div
      sx={{
        flex: '0 0 10em',
        minHeight: '100px',
        display: 'flex',
        alignItems: 'flex-end',
        m: 3,
      }}
    >
      <div
        sx={{
          transition: 'transform 0.3s ease-in-out',
          transform: `translate(${dx}px, ${dy}px)`,
        }}
      >
        <ValueContainer onDrop={(): void => props.onDrop('left')}>
          {props.left !== null && props.left !== undefined ? (
            <Value
              hidden={props.hidden}
              onBegin={(): void => {
                if (props.left) {
                  props.onDrag(props.left)
                }
              }}
              onEnd={(): void => props.onDrag()}
              value={props.left}
            />
          ) : null}
        </ValueContainer>
      </div>
      <div
        sx={{
          width: '100px',
          height: '2px',
          bg: 'muted',
          transition: 'transform 0.3s ease-in-out',
          transform: `rotate(${deg}deg)`,
        }}
      />
      <div
        sx={{
          transition: 'transform 0.3s ease-in-out',
          transform: `translate(${-dx}px, ${-dy}px)`,
        }}
      >
        <ValueContainer onDrop={(): void => props.onDrop('right')}>
          {props.right !== null && props.right !== undefined ? (
            <Value
              hidden={props.hidden}
              onBegin={(): void => {
                if (props.right) {
                  props.onDrag(props.right)
                }
              }}
              onEnd={(): void => props.onDrag()}
              value={props.right}
            />
          ) : null}
        </ValueContainer>
      </div>
    </div>
  )
}
