export function randomInt(bound: number): number {
  return Math.floor(Math.random() * Math.floor(bound))
}

export function randomNumbers(count: number, max = 10): number[] {
  const result = []
  for (let i = 0; i < count; i++) {
    result.push(randomInt(max))
  }

  return result
}
