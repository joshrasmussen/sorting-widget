import {
  ADD_ARRAY,
  UPDATE_ARRAY,
  ADD_VARIABLE,
  UPDATE_VARIABLE,
  DELETE_VARIABLE,
  SET_DRAGGED_VALUE,
  INCREMENT_MOVES,
  TOGGLE_ARRAY_VALUES,
  UPDATE_ARRAY_NAME,
  UPDATE_VARIABLE_NAME,
  UPDATE_SCALE,
} from './constants'

export type ArrayState = {
  identifier: string
  showValues: boolean
  value: number[]
  final: number[]
}

export type VariableState = {
  identifier: string
  value: number
  index?: number
}

export type AppState = {
  arrays: ArrayState[]
  variables: VariableState[]
  draggedValue?: VariableState
  numMoves: number
  scale: {
    left?: VariableState
    right?: VariableState
  }
}

export type IncreaseNumMovesAction = {
  type: typeof INCREMENT_MOVES
}

export type SetDraggedValueAction = {
  type: typeof SET_DRAGGED_VALUE
  value?: VariableState
}

export type AddArrayAction = {
  type: typeof ADD_ARRAY
  name: string
  size: number
}

export type UpdateArrayAction = {
  type: typeof UPDATE_ARRAY
  name: string
  value: number
  index: number
}

export type UpdateArrayNameAction = {
  type: typeof UPDATE_ARRAY_NAME
  new: string
  old: string
}

export type ToggleArrayValuesAction = {
  type: typeof TOGGLE_ARRAY_VALUES
  name: string
}

export type ArrayActions =
  | AddArrayAction
  | UpdateArrayAction
  | ToggleArrayValuesAction
  | UpdateArrayNameAction

export type UpdateVariableAction = {
  type: typeof UPDATE_VARIABLE
  name: string
  value: number
  index?: number
}

export type UpdateVariableNameAction = {
  type: typeof UPDATE_VARIABLE_NAME
  new: string
  old: string
}

export type AddVariableAction = {
  type: typeof ADD_VARIABLE
  name: string
}

export type DeleteVariableAction = {
  type: typeof DELETE_VARIABLE
  name: string
}

export type VariableActions =
  | UpdateVariableAction
  | AddVariableAction
  | DeleteVariableAction
  | UpdateVariableNameAction

export type UpdateScaleAction = {
  type: typeof UPDATE_SCALE
  side: 'left' | 'right'
  value: number | null
  index?: number
  name: string
}

export type AppActions =
  | ArrayActions
  | VariableActions
  | SetDraggedValueAction
  | IncreaseNumMovesAction
  | UpdateScaleAction
