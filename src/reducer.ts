import {
  ADD_ARRAY,
  UPDATE_ARRAY,
  ADD_VARIABLE,
  UPDATE_VARIABLE,
  DELETE_VARIABLE,
  SET_DRAGGED_VALUE,
  INCREMENT_MOVES,
  TOGGLE_ARRAY_VALUES,
  UPDATE_ARRAY_NAME,
  UPDATE_VARIABLE_NAME,
  UPDATE_SCALE,
} from './constants'
import { AppState, AppActions, AddArrayAction, ArrayState } from './types'
import { randomNumbers } from './utils'

function makeArrayState(action: AddArrayAction): ArrayState {
  const value = randomNumbers(action.size)
  return {
    identifier: action.name,
    value,
    showValues: false,
    final: [...value].sort(),
  }
}

export function reducer(state: AppState, action: AppActions): AppState {
  switch (action.type) {
    case UPDATE_SCALE:
      return {
        ...state,
        scale: {
          ...state.scale,
          [action.side]: {
            identifier: action.name,
            value: action.value,
            index: action.index,
          },
        },
      }
    case INCREMENT_MOVES:
      return {
        ...state,
        numMoves: state.numMoves + 1,
      }
    case SET_DRAGGED_VALUE:
      return {
        ...state,
        draggedValue: action.value,
      }
    case TOGGLE_ARRAY_VALUES:
      return {
        ...state,
        arrays: state.arrays.map(a => {
          if (a.identifier === action.name) {
            return {
              ...a,
              showValues: !a.showValues,
            }
          } else {
            return a
          }
        }),
      }
    case ADD_ARRAY:
      return {
        ...state,
        arrays: [...state.arrays, makeArrayState(action)],
      }
    case UPDATE_ARRAY:
      return {
        ...state,
        arrays: state.arrays.map(a => {
          if (a.identifier === action.name) {
            return {
              ...a,
              value: a.value.map((v, i) => {
                if (i === action.index) {
                  return action.value
                } else {
                  return v
                }
              }),
            }
          } else {
            return a
          }
        }),
      }
    case UPDATE_ARRAY_NAME:
      return {
        ...state,
        arrays: state.arrays.map(a => {
          if (a.identifier === action.old) {
            return {
              ...a,
              identifier: action.new,
            }
          } else {
            return a
          }
        }),
      }
    case ADD_VARIABLE:
      return {
        ...state,
        variables: [
          ...state.variables,
          {
            identifier: action.name,
            value: 0,
          },
        ],
      }
    case UPDATE_VARIABLE:
      return {
        ...state,
        variables: state.variables.map(v => {
          if (v.identifier === action.name) {
            return {
              ...v,
              value: action.value,
              index: action.index,
            }
          } else {
            return v
          }
        }),
      }
    case UPDATE_VARIABLE_NAME:
      return {
        ...state,
        variables: state.variables.map(v => {
          if (v.identifier === action.old) {
            return {
              ...v,
              identifier: action.new,
            }
          } else {
            return v
          }
        }),
      }
    case DELETE_VARIABLE:
      return {
        ...state,
        variables: state.variables.filter(v => {
          return v.identifier !== action.name
        }),
      }
    default:
      return state
  }
}
