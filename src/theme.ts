import { Theme } from 'theme-ui'

export const theme: Theme = {
  fonts: {
    body: 'Helvetica, sans-serif',
    heading: 'Helvetica, sans-serif',
  },
  space: [0, 4, 8, 16, 32, 64, 128, 256, 512],
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 96],
  fontWeights: {
    body: 400,
    heading: 700,
  },
  colors: {
    primary: '#b12121',
    secondary: '#1cffc9',
    text: '#000',
    background: '#fff',
    muted: '#ccc',
  },
  styles: {
    root: {
      fontFamily: 'body',
      fontWeight: 'body',
      fontSize: 3,
    },
  },
}
