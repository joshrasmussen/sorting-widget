/** @jsx jsx */
import { ReactElement, ReactNode } from 'react'
import { jsx } from 'theme-ui'

type VariableContainerProps = {
  /** */
  name: string

  children?: ReactNode
}

export function VariableContainer(props: VariableContainerProps): ReactElement {
  return (
    <div
      sx={{
        m: 3,
      }}
    >
      <div>{props.name}</div>
      <div
        sx={{
          display: 'flex',
          flexGrow: 1,
          flexDirection: 'column',
        }}
      >
        {props.children}
      </div>
    </div>
  )
}
