/** @jsx jsx */
import { ReactElement, useReducer, useEffect } from 'react'
import { jsx, ThemeProvider } from 'theme-ui'
import { DndProvider } from 'react-dnd'
import Backend from 'react-dnd-html5-backend'

import { reducer } from './reducer'
import { theme } from './theme'
import { Heading } from './Heading'
import { Scale } from './Scale'
import { VariableContainer } from './VariableContainer'
import {
  ADD_ARRAY,
  UPDATE_ARRAY,
  UPDATE_VARIABLE,
  ADD_VARIABLE,
  SET_DRAGGED_VALUE,
  INCREMENT_MOVES,
  UPDATE_SCALE,
} from './constants'
import { ValueContainer } from './ValueContainer'
import { Value } from './Value'

function isSame(a: number[], b: number[]): boolean {
  for (let i = 0; i < a.length; i++) {
    if (a[i] !== b[i]) {
      return false
    }
  }

  return true
}

export function App(): ReactElement {
  const [state, dispatch] = useReducer(reducer, {
    arrays: [],
    variables: [],
    numMoves: 0,
    scale: {},
  })

  useEffect(() => {
    dispatch({ type: ADD_ARRAY, name: 'array', size: 8 })
    dispatch({ type: ADD_VARIABLE, name: 'temp1' })
  }, [])

  return (
    <DndProvider backend={Backend}>
      <ThemeProvider theme={theme}>
        <Heading>Sorting!</Heading>
        {state.arrays.map((a, i) => {
          const isFinished = isSame(a.final, a.value)
          return (
            <VariableContainer key={`${a.identifier}-${i}`} name={a.identifier}>
              <div
                sx={{
                  display: 'flex',
                }}
              >
                {a.value.map((v, i) => {
                  const highlight = [
                    ...state.variables
                      .filter(sv => i === sv.index)
                      .map(v => v.identifier),
                  ]
                  if (
                    state.scale.left?.identifier === a.identifier &&
                    state.scale.left?.index === i
                  ) {
                    highlight.push('left')
                  }
                  if (
                    state.scale.right?.identifier === a.identifier &&
                    state.scale.right?.index === i
                  ) {
                    highlight.push('right')
                  }

                  return (
                    <ValueContainer
                      highlight={highlight.join(', ')}
                      key={`${v}-${i}`}
                      onDrop={(): void => {
                        if (
                          state.draggedValue !== undefined &&
                          state.draggedValue !== null
                        ) {
                          dispatch({
                            type: UPDATE_ARRAY,
                            name: a.identifier,
                            value: state.draggedValue.value,
                            index: i,
                          })
                          dispatch({ type: INCREMENT_MOVES })
                        }
                      }}
                    >
                      <Value
                        hidden={!isFinished}
                        onBegin={(): void =>
                          dispatch({
                            type: SET_DRAGGED_VALUE,
                            value: {
                              index: i,
                              value: v,
                              identifier: a.identifier,
                            },
                          })
                        }
                        onEnd={(): void =>
                          dispatch({ type: SET_DRAGGED_VALUE })
                        }
                        value={v}
                      />
                    </ValueContainer>
                  )
                })}
              </div>
            </VariableContainer>
          )
        })}

        <div sx={{ display: 'flex' }}>
          {state.variables.map((v, i) => (
            <VariableContainer key={`${v.identifier}-${i}`} name={v.identifier}>
              <ValueContainer
                onDrop={(): void => {
                  if (
                    state.draggedValue !== null &&
                    state.draggedValue !== undefined
                  ) {
                    dispatch({
                      type: UPDATE_VARIABLE,
                      name: v.identifier,
                      value: state.draggedValue.value,
                      index: state.draggedValue.index,
                    })
                    dispatch({ type: INCREMENT_MOVES })
                  }
                }}
              >
                <Value
                  hidden
                  onBegin={(): void =>
                    dispatch({ type: SET_DRAGGED_VALUE, value: v })
                  }
                  onEnd={(): void => dispatch({ type: SET_DRAGGED_VALUE })}
                  value={v.value}
                />
              </ValueContainer>
            </VariableContainer>
          ))}
        </div>
        <div sx={{ display: 'flex' }}>
          <Scale
            hidden
            left={state.scale.left?.value}
            onDrag={(value): void => {
              if (value) {
                dispatch({
                  type: SET_DRAGGED_VALUE,
                  value: { value, identifier: 'left' },
                })
              } else {
                dispatch({ type: SET_DRAGGED_VALUE })
              }
            }}
            onDrop={(side): void => {
              if (
                state.draggedValue !== null &&
                state.draggedValue !== undefined
              ) {
                dispatch({
                  type: UPDATE_SCALE,
                  side,
                  value: state.draggedValue.value,
                  index: state.draggedValue.index,
                  name: state.draggedValue?.identifier,
                })
              }
            }}
            right={state.scale.right?.value}
          />
        </div>
      </ThemeProvider>
    </DndProvider>
  )
}
