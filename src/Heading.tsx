/** @jsx jsx */
import { ReactElement, ReactNode } from 'react'
import { jsx, Styled } from 'theme-ui'

type HeadingProps = {
  children?: ReactNode
}

export function Heading(props: HeadingProps): ReactElement {
  return (
    <header
      sx={{
        p: 1,
        backgroundColor: 'primary',
        color: 'background',
        textAlign: 'center',
      }}
    >
      <Styled.h1>{props.children}</Styled.h1>
    </header>
  )
}
