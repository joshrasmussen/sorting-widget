/** @jsx jsx */
import { ReactElement } from 'react'
import { jsx } from 'theme-ui'
import { useDrag } from 'react-dnd'
import { ItemTypes } from './constants'

type ArrayElementProps = {
  onBegin: (value: number) => void
  onEnd: () => void
  value: number
  hidden?: boolean
}

export function Value(props: ArrayElementProps): ReactElement {
  const [{ isDragging }, drag] = useDrag({
    item: { type: ItemTypes.ELEMENT },
    begin() {
      props.onBegin(props.value)
    },
    end() {
      props.onEnd()
    },
    collect(monitor) {
      return {
        isDragging: !!monitor.isDragging(),
      }
    },
  })

  return (
    <div
      ref={drag}
      sx={{
        p: 3,
        bg: props.hidden ? 'primary' : 'background',
        opacity: isDragging ? 0.5 : 1,
        color: props.hidden ? 'background' : 'text',
        textAlign: 'center',
        fontWeight: 'bold',
        width: '100px',
        height: '100px',
        cursor: 'move',
        mx: 'auto',
        flexGrow: 1,
      }}
    >
      {!props.hidden ? props.value : '?'}
    </div>
  )
}
