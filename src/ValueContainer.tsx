/** @jsx jsx */
import { ReactElement, ReactNode } from 'react'
import { jsx, Theme } from 'theme-ui'
import { useDrop } from 'react-dnd'
import { ItemTypes } from './constants'

type ValueContainerProps = {
  onDrop: () => void

  highlight?: ReactNode

  /** */
  children?: ReactNode
}

export function ValueContainer(props: ValueContainerProps): ReactElement {
  const [{ isOver }, drop] = useDrop({
    accept: ItemTypes.ELEMENT,
    drop() {
      props.onDrop()
    },
    collect(monitor) {
      return {
        isOver: !!monitor.isOver(),
      }
    },
  })

  return (
    <div
      ref={drop}
      sx={{
        border: (theme: Theme): string => `0.5px solid ${theme.colors?.muted}`,
        p: 3,
        flexGrow: 1,
        bg: isOver ? 'muted' : 'background',
      }}
    >
      {props.children}
      <div sx={{ textAlign: 'center', m: 2, fontWeight: 'bold' }}>
        {props.highlight}
      </div>
    </div>
  )
}
